```mermaid
classDiagram

class ReportController{
    - Facade: ReportFacade
    + MakeReport(employee: Employee, reportType: string): void
    + ReportController()
}

class IReportHandler{
    <<Interface>>
    NextHandler: IReportHandler
    Handle(report: IReport): void
}

class ReportShowHandler{
    + NextHandler: IReportHandler
    + Handle(report: IReport): void
    + ReportShowHandler()
}
class ReportSaveHandler{
    + NextHandler: IReportHandler
    + ReportSaveHandler()
    + Handle(report: IReport): void
}
class ReportFacade{
    - ReportBuilder: Builder
    - ReportHandler: IReportHandler
    + ReportFacade(reportBuilder: Builder)
    + MakeReport(): void
}
class Employee{
    + Name: string
    + Salary: decimal
    + Employee(name: string, salary: decimal)
}
class PDFReport{
    + Header: string
    + Body: string
    + Footer: string
    + GetInfo(): string
    + PDFReport()
}
class IReport{
    <<Interface>>
    Header: string
    Body: string
    Footer: string
    + GetInfo(): string
}

class Builder{
    <<Abstract>>
    + Report: IReport
    # Employee: Employee
    + BuildHeader(): void
    + BuildBody(): void
    + BuildFooter(): void
    + Builder(employee: Employee)
}

class PDFReportBuilder{
    + PDFReportBuilder(emloyee: Employee)
    + BuildHeader(header: string): void
    + BuildBody(body: string): void
    + BuildFooter(footer: string): void
}



PDFReportBuilder --|> Builder
PDFReportBuilder ..> PDFReport
PDFReport --|> IReport
ReportShowHandler --|> IReportHandler
ReportSaveHandler --|> IReportHandler
Builder ..> IReport
Builder --> Employee
ReportFacade --> Builder
ReportFacade ..> IReport
ReportFacade --> IReportHandler
ReportController --> ReportFacade
ReportController ..> IReport
```