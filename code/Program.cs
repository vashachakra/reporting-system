﻿namespace code;

interface IReport
{
    string Header {get;set;}
    string Body {get;set;}
    string Footer{get;set;}
    string GetInfo();
}
class PDFReport : IReport
{
    public string Header {get;set;}
    public string Body {get;set;}
    public string Footer {get;set;}
    public string GetInfo() => $"{Header}\n{Body}\n{Footer}"; 
}
interface IReportHandler
{
    IReportHandler NextHandler {get;set;}
    void Handle(IReport report);
}
class ReportShowHandler : IReportHandler 
{
    public IReportHandler? NextHandler {get;set;}
    public void Handle(IReport report)
    {
        System.Console.WriteLine(report.GetInfo());
        NextHandler?.Handle(report);
    }
}
class ReportSaveHandler : IReportHandler
{
    public IReportHandler NextHandler {get;set;}
    public ReportSaveHandler() => NextHandler = new ReportShowHandler();
    public void Handle(IReport report)
    {
        System.Console.WriteLine("*Отчет сохранен");
        NextHandler?.Handle(report);
    }
}
abstract class Builder
{
    public IReport Report {get;set;}
    protected readonly Employee Employee;
    public Builder(Employee employee)
    {
        Employee = employee;
    }
    public abstract void BuildHeader();
    public abstract void BuildBody();
    public abstract void BuildFooter(); 
}
class PDFReportBuilder : Builder
{
    public PDFReportBuilder(Employee employee) : base(employee) =>
        Report = new PDFReport();
    public override void BuildHeader() =>
        Report.Header = $"ОТЧЕТ О СОТРУДНИКЕ {Employee.Name}\n{new string('-',20)}";
    public override void BuildBody() =>
        Report.Body = $"ЗП: {Employee.Salary}\n{new string('-',20)}";
    public override void BuildFooter() =>
        Report.Footer = $"{DateTime.Now.Year}-{DateTime.Now.Month}-{DateTime.Now.Day}";
}
class Employee
{
    public readonly string Name;
    public readonly decimal Salary;
    public Employee(string name, decimal salary)
    {
        Name = name;
        Salary = salary;
    }
}
class ReportFacade
{
    private Builder ReportBuilder;
    private IReportHandler ReportHandler;
    public ReportFacade(Builder reportBuilder)
    {
        ReportBuilder = reportBuilder;
        ReportHandler = new ReportSaveHandler();
    }
    public void MakeReport(Employee employee, string reportType)
    {
        ReportBuilder.BuildHeader();
        ReportBuilder.BuildBody();
        ReportBuilder.BuildFooter();

       // IReport report = ReportBuilder.Report;

        //ReportHandler.Handle(report);
    }
}
class ReportController
{
    private IReport report;
    private ReportFacade Facade;
    public void MakeReport(Employee employee, string reportType) 
    {
        Builder builder;
        switch(reportType.ToLower())
        {
            case "pdf":
                builder = new PDFReportBuilder(employee);
                break;
            default:
                return;
        }
        Facade = new ReportFacade(builder);
        Facade.MakeReport(employee, reportType);
        report = builder.Report;

    }
    public void HandleReport(){

        IReportHandler handler1 = new ReportShowHandler();
        IReportHandler handler2 = new ReportSaveHandler();
        handler1.NextHandler = handler2;
        handler1.Handle(report);

    }
}
class Program
{
    static void Main()
    {
        Employee employee = new Employee("Корягин Сергей Викторович", 0);
        ReportController controller = new();
        controller.MakeReport(employee, "pdf");
        controller.HandleReport();
    }
}
